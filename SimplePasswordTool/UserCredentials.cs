﻿using System;
using System.Runtime.InteropServices;

namespace SimplePasswordTool
{
    class UserCredentials
    {

        // We will use the NetUSerChangePassword function
        // https://msdn.microsoft.com/en-us/library/windows/desktop/aa370650(v=vs.85).aspx
        [DllImport("Netapi32.dll")]
        extern static int NetUserChangePassword(
            [MarshalAs(UnmanagedType.LPWStr)]string domainname,
            [MarshalAs(UnmanagedType.LPWStr)]string username,
            [MarshalAs(UnmanagedType.LPWStr)]string oldpassword,
            [MarshalAs(UnmanagedType.LPWStr)]string newpassword);

        // Prevent calling the class without parameters.
        private UserCredentials() { }

        public UserCredentials(string domain, string username, string oldpassword, string newpassword)
        {
            DomainName = domain;
            UserName = username;
            OldPassword = oldpassword;
            NewPassword = newpassword;
        }

        private string DomainName { get; set; }
        private string UserName { get; set; }
        private string OldPassword { get; set; }
        private string NewPassword { get; set; }
        private int Result { get; set; }

        /// <summary>
        /// Change password with use of NetUserChangePassword function.
        /// </summary>
        public void ChangePassword()
        {
            Result = NetUserChangePassword(DomainName, UserName, OldPassword, NewPassword);
        }

        /// <summary>
        /// Return string array of [0]domain [1]user
        /// </summary>
        /// <returns>string[]</returns>
        public static string[] GetCredentials(string credentials)
        {
            string[] credArray;
            string user = string.Empty;
            char backslash = '\\';

            if (!String.IsNullOrEmpty(credentials))
            {
                user = credentials;
            }

            if (user.Split(backslash).Length == 1)
            {
                user = Environment.GetEnvironmentVariable("userdomain") + "\\" + user;
            }

            return credArray = IsValidUserCredential(user) ? user.Split(backslash): GetCurrentUserCredentials().Split(backslash);
        }

        /// <summary>
        /// Returns true when valid credentials.
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsValidUserCredential(string credentials)
        {
            if ((string.IsNullOrWhiteSpace(credentials)) || (string.IsNullOrEmpty(credentials)))
            {
                return false;
            }
            else if ((credentials.Contains("/")) || (credentials.Equals(".")) || credentials.Equals("\\"))
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Return environment variables for current user logged on the system.
        /// </summary>
        /// <returns>string</returns>
        public static string GetCurrentUserCredentials()
        {
            return string.Format(Environment.GetEnvironmentVariable("userdomain") + "\\" + Environment.GetEnvironmentVariable("username"));
        }

        /// <summary>
        /// Return a string dependent on the Result of the NetUserChangePassword call.
        /// </summary>
        /// <returns>string</returns>
        public string GetResultMessage()
        {
            string message;

            if (Result != 0)
            {
                // Unfortunatly we need to return a custom string based on the Result for .NET
                // does not support something like Err.Message as in VB6.
                switch (Result)
                {
                    case 5:
                        message = string.Format("Access denied. Try run this program with elevated priviliges.");
                        break;
                    case 86:
                        message = string.Format("Old password is incorrect.");
                        break;
                    case 615:
                        message = string.Format("The password provided is too short to meet the policy of your user account. Please choose a longer password.");
                        break;
                    case 616:
                        message = string.Format("The policy of your user account does not allow you to change passwords too frequently. This is done to prevent users from changing back to a familiar, but potentially discovered, password. If you feel your password has been compromised then please contact your administrator immediately to have a new one assigned.");
                        break;
                    case 617:
                        message = string.Format("You have attempted to change your password to one that you have used in the past.The policy of your user account does not allow this.Please select a password that you have not previously used.");
                        break;
                    case 657:
                        message = string.Format("The password provided is too long to meet the policy of your user account. Please choose a shorter password.");
                        break;
                    case 1351:
                        message = string.Format("Domain cannot be found.");
                        break;
                    case 1908:
                        message = string.Format("Could not find the domain controller for this domain.");
                        break;
                    case 2221:
                        message = string.Format("Unknown username");
                        break;
                    case 2254:
                        message = string.Format("The new password is too short.");
                        break;
                    case 8646:
                        // We need to do this through DirectoryServices
                        message = string.Format("The system is not authoritative for the specified account and therefore cannot complete the operation. Please retry the operation using the provider associated with this account. If this is an online provider please use the provider's online site.");
                        break;
                    default:
                        message = string.Format("Something went wrong. Errorcode: {0}", Result.ToString());
                        break;
                }
            }
            else
            {
                message = "Your password has been changed!";
            }
            return message;
        }        
    }
}
