﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePasswordTool
{
    public partial class frmMain : Form
    {
        string[] credentialArray;
        public frmMain()
        {
            InitializeComponent();
            txtDomainUsernam.Text = UserCredentials.GetCurrentUserCredentials();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            bool changed = false;
            string resultMessage = string.Empty;
            // Check if all textboxes contains text.
            if (IsFilled(txtDomainUsernam.Text) && IsFilled(txtNewPassword.Text)
                && IsFilled(txtOldPassword.Text) && IsFilled(txtRepeatPassword.Text))
            {
                // In case the credentials are changed in the textbox..
                credentialArray = UserCredentials.GetCredentials(txtDomainUsernam.Text);
                // Show credentials in the user textbox
                txtDomainUsernam.Text = string.Format(credentialArray[0] + "\\" + credentialArray[1]);

                string domain = credentialArray[0];
                string username = credentialArray[1];
                string oldpassword = txtOldPassword.Text;
                string newpassword = txtNewPassword.Text;
                string confirmpassword = txtRepeatPassword.Text;

                if (newpassword.Equals(confirmpassword))
                {
                    UserCredentials userCred = new UserCredentials(domain, username, oldpassword, newpassword);
                    userCred.ChangePassword();
                    resultMessage = userCred.GetResultMessage();
                    changed = true;
                }
                else
                {
                    MessageBox.Show("New passwords are not the same!", "Error");
                    ClearPasswordFields();
                }
            }
            else
            {
                MessageBox.Show("Please fill in all fields.", "Error");
                ClearPasswordFields();
            }

            if (changed)
            {
                MessageBox.Show(resultMessage, "Info");
                ClearPasswordFields();
            }
        }

        private bool IsFilled(string text)
        {
            bool valid;
            return valid = ((string.IsNullOrWhiteSpace(text)) || (string.IsNullOrEmpty(text))) ? false : true;
        }

        
        private void ClearPasswordFields()
        {
            txtOldPassword.Text = "";
            txtNewPassword.Text = "";
            txtRepeatPassword.Text = "";
        }
    }
}
